.ONESHELL:
PYTHON=python3
PIP=pip3
PROJECT=footprint_facility

ifeq (, $(shell which $(PYTHON)))
 $(error "No $(PYTHON) in $(PATH), consider doing apt-get install python3.6+")
endif

all: lint test coverage dist
.PHONY: all

venv: venv/bin/activate

venv/bin/activate: requirements.txt
	test -d venv || $(PYTHON) -m venv venv
	. venv/bin/activate
	$(PIP) install wheel
	$(PYTHON) -m pip install -Ur requirements.txt --no-cache-dir
	$(PYTHON) -m pip install -Ur requirements-dev.txt --no-cache-dir
	touch venv/bin/activate

test: venv
	. venv/bin/activate
	$(PYTHON) -m unittest discover
	$(PYTHON) -m unittest -v

clean:
	@rm -rf htmlcov
	@find -iname "*.pyc" -delete
	@find -name "__pycache__" -delete
	@rm -f .coverage coverage.xml
	@rm -rf .htmlcov
	@rm -rf .pytest_cache
	@rm -rf cache
	@rm -rf public_html

coverage: venv
	. venv/bin/activate
	coverage run --include '$(PROJECT)/*'  --omit $(PROJECT)/_version.py  -m unittest -v
	coverage xml
	coverage html


public_html:
	mkdir -p $@


lint: venv
	. venv/bin/activate
	pycodestyle --show-source --show-pep8 --exclude=".eggs,versioneer.py,*_version.py,venv,.venv,docs, sample.py"

dist: venv
	. venv/bin/activate
	$(PYTHON) -m build

dist-deploy: dist
	. venv/bin/activate
	$(PYTHON) -m twine upload --repository ${PROJECT} dist/*

dist-clean: clean
	@rm -rf venv
	@rm -rf build builds dist .eggs $(PROJECT).egg-info

version: setup.cfg venv
	. venv/bin/activate
	versioneer install

license: liccheck.ini requirements.txt
	. venv/bin/activate
	liccheck -s liccheck.ini -r requirements.txt
